<div class="panel panel-primary">
    <div class="panel-heading">Personal information</div>
    <div class="panel-body">
        <div class="form-inline">
            <div class="form-group col-sm-6 removeleft">
                {!! Form::label('first_name', 'First name', ['class' => 'control-label']) !!}
                {!!
                    Form::text('first_name',
                    isset($data['first_name']) ? $data['first_name'] : null,
                    ['class' => 'form-control'])
                !!}
            </div>
            <div class="form-group col-sm-6 removeleft removeright">
                {!! Form::label('last_name', 'Last name', ['class' => 'control-label']) !!}
                {!!
                    Form::text('last_name',
                    isset($data['last_name']) ? $data['last_name'] : null,
                    ['class' => 'form-control'])
                !!}
            </div>
        </div>
        <div class="form-inline">
            <div class="form-group col-sm-6 removeleft">
                {!! Form::label('annual_income', 'Annual Income:', ['class' => 'control-label']) !!}
                {!!
                    Form::select('annual_income',
                    \App\Models\Client::annual_income(),
                    null,
                    ['class' => 'form-control ui search selection top right pointing search-select',
                    'id' => 'search-select'])
                !!}
            </div>
            <div class="form-group col-sm-6 removeleft removeright">
                {!! Form::label('marital_status', 'Marital Status:', ['class' => 'control-label']) !!}
                {!!
                    Form::select('marital_status',
                    \App\Models\Client::marital_status(),
                    null,
                    ['class' => 'form-control ui search selection top right pointing search-select',
                    'id' => 'search-select'])
                !!}
            </div>
        </div>

        <div class="form-inline">
            <div class="form-group col-sm-6 removeleft">
                {!! Form::label('social_security_number', 'Social Security Number:', ['class' => 'control-label']) !!}
                {!!
                    Form::text('social_security_number',
                    isset($data['social_security_number']) ?$data['social_security_number'] : null,
                    ['class' => 'form-control'])
                !!}
            </div>

            <div class="form-group removeleft removeright">
                {!! Form::label('blood_type', 'Blood Type:', ['class' => 'control-label']) !!}
                {!!
                    Form::select('blood_type',
                    \App\Models\Client::blood_type(),
                    null,
                    ['class' => 'form-control ui search selection top right pointing search-select',
                    'id' => 'search-select'])
                !!}
            </div>
        </div>
    </div>

</div>

<div class="panel panel-primary">
    <div class="panel-heading">Contact information</div>
    <div class="panel-body">
        <div class="form-group">
            {!! Form::label('email', 'Email:', ['class' => 'control-label']) !!}
            {!!
                Form::email('email',
                isset($data['email']) ? $data['email'] : null,
                ['class' => 'form-control'])
            !!}
        </div>
        <div class="form-group col-sm-6 removeleft">
            {!! Form::label('primary_number', 'Phone Number:', ['class' => 'control-label']) !!}
            {!!
                Form::text('primary_number',
                isset($data['phone']) ? $data['phone'] : null,
                ['class' => 'form-control'])
            !!}
        </div>

        <div class="form-group col-sm-6 removeleft removeright">
            {!! Form::label('address', 'Post Address:', ['class' => 'control-label']) !!}
            {!!
                Form::text('address',
                isset($data['address']) ? $data['address'] : null,
                ['class' => 'form-control'])
            !!}
        </div>

        <div class="form-inline">
            <div class="form-group col-sm-4 removeleft">
                {!! Form::label('zipcode', 'Zipcode:', ['class' => 'control-label']) !!}
                {!!
                    Form::text('zipcode',
                     isset($data['zipcode']) ? $data['zipcode'] : null,
                     ['class' => 'form-control'])
                !!}
            </div>

            <div class="form-group col-sm-4 removeleft removeright">
                {!! Form::label('city', 'City:', ['class' => 'control-label']) !!}
                {!!
                    Form::text('city',
                    isset($data['city']) ? $data['city'] : null,
                    ['class' => 'form-control'])
                !!}
            </div>

            <div class="form-group col-sm-4 removeright">
                {!! Form::label('country', 'Country:', ['class' => 'control-label']) !!}
                {!!
                    Form::text('country',
                    isset($data['country']) ? $data['country'] : null,
                    ['class' => 'form-control'])
                !!}
            </div>
        </div>

    </div>
</div>

<div class="panel panel-primary">
    <div class="panel-heading">Professional information</div>
    <div class="panel-body">
        <div class="form-group">
            {!! Form::label('company_name', 'Company name:', ['class' => 'control-label']) !!}
            {!!
                Form::text('company_name',
                isset($data['name']) ? $data['name'] : null,
                ['class' => 'form-control'])
            !!}
        </div>
        <div class="form-group">
            {!! Form::label('company_type', 'Company Size:', ['class' => 'control-label']) !!}
            {!!
                Form::select('company_type',
                \App\Models\Client::company_size(),
                null,
                ['class' => 'form-control ui search selection top right pointing search-select',
                'id' => 'search-select'])
            !!}
        </div>
        <div class="form-group">

            {!! Form::label('position', 'Position:', ['class' => 'control-label']) !!}
            {!!
                Form::text('position',
                isset($data['position']) ? $data['position'] : null,
                ['class' => 'form-control'])
            !!}
        </div>
        <div class="form-group">
            {!! Form::label('industry', 'Industry:', ['class' => 'control-label']) !!}
            {!!
                Form::select('industry_id',
                $industries,
                null,
                ['class' => 'form-control ui search selection top right pointing search-select',
                'id' => 'search-select'])
            !!}
        </div>
    </div>
</div>



{{--<div class="form-inline">--}}
    {{--<div class="form-group col-sm-6 removeleft">--}}
        {{--{!! Form::label('vat', 'Vat:', ['class' => 'control-label']) !!}--}}
        {{--{!! --}}
            {{--Form::text('vat',--}}
            {{--isset($data['vat']) ?$data['vat'] : null,--}}
            {{--['class' => 'form-control']) --}}
        {{--!!}--}}
    {{--</div>--}}
{{--</div>--}}







{{--<div class="form-inline">--}}

    {{--<div class="form-group col-sm-6 removeleft removeright">--}}
        {{--{!! Form::label('secondary_number', 'Secondary Number:', ['class' => 'control-label']) !!}--}}
        {{--{!! --}}
            {{--Form::text('secondary_number',  --}}
            {{--null, --}}
            {{--['class' => 'form-control']) --}}
        {{--!!}--}}
    {{--</div>--}}
{{--</div>--}}





<div class="form-group">
    {!! Form::label('user_id', 'Assign user:', ['class' => 'control-label']) !!}
    {!! Form::select('user_id', $users, null, ['class' => 'form-control ui search selection top right pointing search-select', 'id' => 'search-select']) !!}

</div>


{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary']) !!}