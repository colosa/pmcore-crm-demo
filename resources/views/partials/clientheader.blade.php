

    <h1 class="moveup">{{$client->first_name}} {{$client->last_name}} <small>({{$client->company_name}})</small></h1>

    <!--Client info leftside-->
    <div class="contactleft" style="width: 100%;">
        <div class="panel panel-primary col-sm-6" style="padding-left: 0px;">
            <div class="panel-heading"><i class="glyphicon glyphicon-user"></i> Personal information</div>
            <div class="panel-body">
                @if(!empty($client->first_name))<div><label class="">First name:</label> {{$client->first_name}}</div>@endif
                @if(!empty($client->last_name))<div><label>Last name:</label> {{$client->last_name}}</div>@endif
                <div><label>Annual Income:</label> {{$client->annual_income}}</div>
                <div><label>Marital Status:</label> {{$client->marital_status}}</div>
                @if(!empty($client->social_security_number))<div><label>Social Security Number:</label> {{$client->social_security_number}}</div>@endif
                <div><label>Blood Type:</label> {{$client->blood_type}}</div>
            </div>
        </div>

        <div class="panel panel-primary col-sm-6" style="padding-right: 0px;">
            <div class="panel-heading"><i class="glyphicon glyphicon-envelope"></i> Contact information</div>
            <div class="panel-body">
                @if(!empty($client->email)) <div><label class="">E-mail:</label> {{$client->email}}</div>@endif
                @if(!empty($client->primary_number)) <div><label class="">Phone number:</label> {{$client->primary_number}}</div>@endif
                @if(!empty($client->address))<div><label class="">Post Address:</label> {{$client->address}}</div>@endif
                @if(!empty($client->zipcode))<div><label class="">ZipCode:</label> {{$client->zipcode}}</div>@endif
                @if(!empty($client->city))<div><label class="">City:</label> {{$client->city}}</div>@endif
                @if(!empty($client->country))<div><label class="">Country:</label> {{$client->country}}</div>@endif
            </div>
        </div>

        <div class="panel panel-primary col-sm-12" style="padding-right: 0px;padding-left: 0px;">
            <div class="panel-heading"><i class="glyphicon glyphicon-briefcase"></i> Professional information</div>
            <div class="panel-body">
                <div class="col-sm-6">
                    @if(!empty($client->company_name))<div><label class="">Company:</label> {{$client->company_name}}</div>@endif
                @if(!empty($client->company_type))<div><label class="">Company size:</label> {{$client->company_type}}</div>@endif
                </div>
                <div class="col-sm-6">
                @if(!empty($client->position))<div><label class="">Position:</label> {{$client->position}}</div>@endif
                <div><label class="">Industry:</label> {{$client->getIndustryName($client->industry_id)}}</div>
                </div>
            </div>
        </div>




    {{--@if($client->email != "")--}}
                {{--<!--MAIL-->--}}
        {{--<p><span class="glyphicon glyphicon-envelope" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('mail') }}" data-placement="left"> </span>--}}
            {{--<a href="mailto:{{$client->email}}" data-toggle="tooltip" data-placement="left">{{$client->email}}</a></p>--}}
        {{--@endif--}}
        {{--@if($client->primary_number != "")--}}
                {{--<!--Work Phone-->--}}
        {{--<p><span class="glyphicon glyphicon-headphones" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title=" {{ __('Primary number') }} " data-placement="left"> </span>--}}
            {{--<a href="tel:{{$client->work_number}}">{{$client->primary_number}}</a></p>--}}
        {{--@endif--}}
        {{--@if($client->secondary_number != "")--}}
                {{--<!--Secondary Phone-->--}}
        {{--<p><span class="glyphicon glyphicon-phone" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('Secondary number') }}" data-placement="left"> </span>--}}
            {{--<a href="tel:{{$client->secondary_number}}">{{$client->secondary_number}}</a></p>--}}
        {{--@endif--}}
        {{--@if($client->address || $client->zipcode || $client->city != "")--}}
                {{--<!--Address-->--}}
        {{--<p><span class="glyphicon glyphicon-home" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('Full address') }}" data-placement="left"> </span> {{$client->address}}--}}
            {{--<br/>{{$client->zipcode}} {{$client->city}}--}}
        {{--</p>--}}
        {{--@endif--}}
    </div>

    <!--Client info leftside END-->
    <!--Client info rightside-->
    {{--<div class="contactright">--}}
        {{--@if($client->company_name != "")--}}
                {{--<!--Company-->--}}
        {{--<p><span class="glyphicon glyphicon-star" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('Company') }}" data-placement="left"> </span> {{$client->company_name}}</p>--}}
        {{--@endif--}}
        {{--@if($client->vat != "")--}}
                {{--<!--Company-->--}}
        {{--<p><span class="glyphicon glyphicon-cloud" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('vat') }}" data-placement="left"> </span> {{$client->vat}}</p>--}}
        {{--@endif--}}
        {{--@if($client->industry != "")--}}
                {{--<!--Industry-->--}}
        {{--<p><span class="glyphicon glyphicon-briefcase" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('Industry') }}"data-placement="left"> </span> {{$client->industry}}</p>--}}
        {{--@endif--}}
        {{--@if($client->company_type!= "")--}}
                {{--<!--Company Type-->--}}
        {{--<p><span class="glyphicon glyphicon-globe" aria-hidden="true" data-toggle="tooltip"--}}
                 {{--title="{{ __('Company type') }}" data-placement="left"> </span>--}}
            {{--{{$client->company_type}}</p>--}}
        {{--@endif--}}
    {{--</div>--}}
    <div class="col-md-12">
    <table class="table">
        <h4>{{ __('All Documents') }}</h4>
        {{--<div class="col-xs-10">
            <div class="form-group">
                <form method="POST" action="{{ url('/clients/upload', $client->id)}}" class="dropzone" id="dropzone"
                      files="true" data-dz-removea
                      enctype="multipart/form-data"
                >
                    <meta name="csrf-token" content="{{ csrf_token() }}">
                </form>
                <p><b>{{ __('Max size') }}</b></p>
            </div>
        </div>--}}
        <thead>
        <tr>
            <th>{{ __('File') }}</th>
            <th>{{ __('Size') }}</th>
            <th>{{ __('Created at') }}</th>
        </tr>
        </thead>
        <tbody>
        @if (count($client->documents) > 0)
        @foreach($client->documents as $document)
            <tr>
                <td><a href="../files/{{$companyname}}/{{$document->path}}"
                       target="_blank">{{$document->file_display}}</a></td>
                <td>{{$document->size}} <span class="moveright"> MB</span></td>
                <td>{{$document->created_at}}</td>

                {{--<td>
                    <form method="POST" action="{{action('DocumentsController@destroy', $document->id)}}">
                        <input type="hidden" name="_method" value="delete"/>
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="submit" class="btn btn-danger" value="Delete"/>
                    </form>
                </td>--}}
            </tr>
        @endforeach
        @endif
        </tbody>
    </table>
    </div>


<!--Client info rightside END-->
