<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Client extends Model
{



    protected $fillable = [
        'first_name',
        'last_name',
        'name',
        'company_name',
        'vat',
        'email',
        'address',
        'zipcode',
        'city',
        'primary_number',
        'secondary_number',
        'industry_id',
        'company_type',
        'user_id',
        'annual_income',
        'marital_status',
        'blood_type',
        'social_security_number',
        'country',
        'position'

        ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

   public function tasks()
    {
        return $this->hasMany(Task::class, 'client_id', 'id')
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc');
    }

    public function leads()
    {
        return $this->hasMany(Lead::class, 'client_id', 'id')
            ->orderBy('status', 'asc')
            ->orderBy('created_at', 'desc');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'client_id', 'id');
    }

    public function invoices()
    {
        return $this->belongsToMany(Invoice::class);
    }

    public function getAssignedUserAttribute()
    {
        return User::findOrFail($this->user_id);
    }

    public function getIndustryName($id)
    {

        return \App\Models\Industry::whereId($id)->pluck('name')->first();
    }

    public static function annual_income()
    {
        return [
            'less than $25000' => 'less than $25000',
            '$25000 - $50000' => '$25000 - $50000',
            '$50000 - $75000' => '$50000 - $75000',
            '$75000 - $100000' => '$75000 - $100000',
            '$100000 - $125000' => '$100000 - $125000',
            'more than $125000' => 'more than $125000'
        ];
    }

    public static function marital_status()
    {
        return [
            'single' => 'single',
            'married' => 'married',
            'widowed' => 'widowed',
            'divorced' => 'divorced',
            'separated' => 'separated',
            'registered partnership' => 'registered partnership'
        ];
    }

    public static function blood_type()
    {
        return [
            'A Positive' => 'A Positive',
            'A Negative' => 'A Negative',
            'A Unknown' => 'A Unknown',
            'B Positive' => 'B Positive',
            'B Negative' => 'B Negative',
            'B Unknown' => 'B Unknown',
            'AB Positive' => 'AB Positive',
            'AB Negative' => 'AB Negative',
            'AB Unknown' => 'AB Unknown',
            'O Positive' => 'O Positive',
            'O Negative' => 'O Negative',
            'O Unknown' => 'O Unknown',
            'Unknown' => 'Unknown'
        ];
    }

    public static function company_size()
    {
        return [
            '<50' => '<50',
            '50-100' => '50-100',
            '100-300' => '100-300',
            '300-500' => '300-500',
            '>500' => '>500'
        ];
    }

}
